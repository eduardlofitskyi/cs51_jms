package com.lofitskyi.producer.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.Queue;

@RestController
public class RestProducer {

    private final Queue queue;

    private final JmsTemplate jmsTemplate;

    @Autowired
    public RestProducer(Queue queue, JmsTemplate jmsTemplate) {
        this.queue = queue;
        this.jmsTemplate = jmsTemplate;
    }

    @GetMapping("/{message}")
    public String sendMessage(@PathVariable String message) {
        jmsTemplate.convertAndSend(queue, message);
        return "Message ['" + message + "'] published successfully";
    }
}
