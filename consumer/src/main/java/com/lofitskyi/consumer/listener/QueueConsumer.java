package com.lofitskyi.consumer.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class QueueConsumer {

    @JmsListener(destination = "standalone.queue")
    public void consume(String message) {
        System.out.println("Received Message: " + message);
    }

}
